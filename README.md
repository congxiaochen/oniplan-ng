# OniplanNg

这是一个Angular入门教程配套的代码，教程地址为：http://blog.csdn.net/column/details/17132.html

## Development server

在\proxy.conf.json中配置了地址映射。
所以使用`npm start`启动服务。
打开浏览器访问`http://localhost:4200/`。

## Code scaffolding

执行 `ng generate component component-name` 去创建一个新的组件。
你还可以像这样使用，创建其他的Angular元素。
`ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

在使用Angular CLI遇到问题可以执行 `ng help` 查看帮助。
在使用本代码时遭遇问题，可以到码云上提交[ISS](https://gitee.com/xiaohuOni/oniplan-ng/issues)
