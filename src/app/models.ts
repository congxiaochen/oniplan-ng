export class SiteStatus{
    users:any;
    visitors:string;
    articles:string;
    comments:string;
    maxOnlineNum:string;
    maxOnlineTime:number;
    onlineNum:string;
    onlineUsers:string;
};
export class User{
    name:string;
    age:number;
}
/**
 * 将大的数据中包含小数据的内容提取出来
 * 接口只是 TypeScript 设计时 (design-time) 的概念。
 * JavaScript 没有接口。 
 * TypeScript 接口不会出现在生成的 JavaScript 代码中。
 * 在运行期，没有接口类型信息可供 Angular 查找。
 * 所以对于一个定义了但是没有初始化的类型数据来说。
 * 它其实是一个空数据。
 * @param data 大的数据
 * @param some 某种类型的数据
 * @return 跟some同类型的数据
 */
export function checkData<T>(data:any,some:T):T{
    for(let prameName in some){
        if(data.hasOwnProperty(prameName)){
            some[prameName] = data[prameName];            
        }else{
            console.log(`no find prameName:${prameName}`);
        }
    }
    return some;
}
