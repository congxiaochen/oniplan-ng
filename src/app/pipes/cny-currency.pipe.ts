import { Pipe, PipeTransform } from '@angular/core';
/**
 * 在字符串或者数字的前方加上人民币符号¥
 */
@Pipe({
  name: 'cnyCurrency'
})
export class CnyCurrencyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return '¥'+value;
  }

}
