import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  article_id:string;

  constructor(private route:ActivatedRoute) {
    route.params.subscribe(params=>{
      this.article_id = params['article_id'];
      console.log(`article_id is ${this.article_id}`);
    })
   }

  ngOnInit() {
  }

}
