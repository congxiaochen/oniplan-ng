import { Directive,ElementRef,HostListener,Input } from '@angular/core';

@Directive({
  selector: '[appOniBigSize]'
})
export class OniBigSizeDirective {
  
  constructor(private el: ElementRef) {
    this.el.nativeElement.style.fontSize = this.initFontSize+'px';
  }

  @Input() initFontSize:number;
  @Input() bigFontSize:number;
  @HostListener('mouseenter') onMouseEnter() {
    this.el.nativeElement.style.fontSize = this.bigFontSize+'px';
  }
  
  @HostListener('mouseleave') onMouseLeave() {
    this.el.nativeElement.style.fontSize = this.initFontSize+'px';
  }
}
