import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  user:User;
  data:{
    name:string,
    age:number,
    some:string,
    other:string
  }
  constructor() { 
    this.data = {
      name:'xiaohuoni',
      age:1,
      some:'haha',
      other:'madas'
    }
    // this.user = {
    //   name:'',
    //   age:0,
    // }
  }
  ngOnInit() {
    let newUser = this.data as User;
    
    // let newUser = checkData(this.data,this.user);
    console.log(newUser);
    //i wand console is 
    /*{
       name:'xiaohuoni',
       age:1
    }*/
  }
}
export class User{
  name:string;
  age:number;
}
export function checkData<T>(data:any,some:T):T{
  for(let prameName in some){
      some[prameName] = data[prameName];
  }
  return some;
}
