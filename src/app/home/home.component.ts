import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router,Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { FollowComponent } from './follow/follow.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
  }

}
export const childRoutes:Routes=[
  {path:'',redirectTo:'main',pathMatch:'full'},
  {path:'main',component:MainComponent},
  {path:'follow',component:FollowComponent}
]
