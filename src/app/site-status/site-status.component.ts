import { Component, OnInit } from '@angular/core';
import { Http , Response ,Headers} from '@angular/http';
import { HmacSHA256 ,SHA256} from 'crypto-js';
import { SiteStatus ,checkData} from '../models';
@Component({
  selector: 'app-site-status',
  templateUrl: './site-status.component.html',
  styleUrls: ['./site-status.component.css']
})
export class SiteStatusComponent implements OnInit {
  siteStatus:SiteStatus;
  itemList:object[];
  constructor(private http:Http) { 
    this.siteStatus = {
      users:'',
      visitors:'',
      articles:'',
      comments:'',
      maxOnlineNum:'',
      maxOnlineTime:0,
      onlineNum:'',
      onlineUsers:''
    };
    this.itemList = [{value:1},{value:2}]
  }
  clickItem(index):void{
    console.log(this.itemList[index]);
  }
  trackByValue(index: number, item:any): number { return item.value; }
  ngOnInit() {
    // var logtime = new Date().getTime();    
    // var logpwd = SHA256('xiaohu123').toString();
    // var logpwdd = HmacSHA256(logpwd,"jsGen").toString();
    // var logpwddd = HmacSHA256(logpwdd,"448627662@qq.com"+":"+logtime).toString();
    // console.log(logpwddd);
    // // 密码：HmacSHA256(user.passwd, loginObj.logname + ':' + loginObj.logtime
    // this.http.post('/api/user/login',{
    //   logauto:true,
    //   logname:"448627662@qq.com",
    //   logpwd:logpwddd,
    //   logtime:logtime
    // }).subscribe((res:Response)=>{
    //   console.log(res.json());
    //   let error = res.json().error;
    //   if(error){
    //     console.error(error);
    //     return false;
    //   }else{
    //     return true;
    //   }
    // });
    this.http.request('/api/index')
    .subscribe((res:Response)=>{
      var data = res.json().data;
      this.siteStatus = checkData(data,this.siteStatus);
    });
    //post
    // this.http.post('/api/index',
    // {
    //   name:"xiaohuoni",
    //   author:"mangfu"
    // }).subscribe((res:Response)=>{})

    //set headers
    // let header:Headers = new Headers();
    // header.append('ONI-PLAN','MANGFU');
    // this.http.get('/api/index',{
    //   headers:header
    // })
    // .subscribe((res:Response)=>{
    //   //do something
    // })
  
  }
}
