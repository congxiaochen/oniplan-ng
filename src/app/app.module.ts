import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SiteStatusComponent } from './site-status/site-status.component';
import { MainNavbarComponent } from './main-navbar/main-navbar.component';
import { HttpModule } from '@angular/http';
import { RouterModule , Routes } from '@angular/router';
import { LatestComponent } from './latest/latest.component';
import { HotsComponent } from './hots/hots.component';
import { UpdateComponent } from './update/update.component';
import { LocationStrategy ,HashLocationStrategy , APP_BASE_HREF} from '@angular/common';
import { ArticleComponent } from './article/article.component';
import { LoginGuard } from './guards/login.guard';
import { HomeComponent ,childRoutes} from './home/home.component';
import { MainComponent } from './home/main/main.component';
import { FollowComponent } from './home/follow/follow.component';
import { CnyCurrencyPipe } from './pipes/cny-currency.pipe';
import { OniBigSizeDirective } from './directive/oni-big-size.directive';
const routes:Routes = [
  {path:'',redirectTo:'latest',pathMatch: 'full' },
  {path:'latest',component:LatestComponent},
  {path:'hots',component:HotsComponent},
  {path:'update',component:UpdateComponent,canActivate:[LoginGuard]},
  {path:'article/:article_id',component:ArticleComponent},
  {path:'home',component:HomeComponent,children:childRoutes}
];

@NgModule({
  declarations: [
    AppComponent,
    SiteStatusComponent,
    MainNavbarComponent,
    LatestComponent,
    HotsComponent,
    UpdateComponent,
    ArticleComponent,
    HomeComponent,
    MainComponent,
    FollowComponent,
    CnyCurrencyPipe,
    OniBigSizeDirective
    ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    LoginGuard,
    {provide:LocationStrategy,useClass:HashLocationStrategy},
    {provide:APP_BASE_HREF,useValue:'/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

